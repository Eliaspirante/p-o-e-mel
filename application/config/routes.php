<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'dashboard_c';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route["dashboard_orders"] = 'dashboard_c/orders';

$route["entrar"] = "login_c";
$route["login"] = "login_c/doLogin";
$route["logout"] = "login_c/doLogout";

$route["dashboard"] = "dashboard_c";

$route['order'] = 'order_c/index';
$route['order/(:num)'] = 'order_c/show/$1';
$route['order/new'] = 'order_c/add';
$route['order/edit/(:num)'] = 'order_c/edit/$1';

$route['updateorder'] = 'order_c/closeOrder';
$route['updateitemorder'] = 'order_c/updateItemOrder';
$route['printorder/(:num)'] = 'order_c/printOrder/$1';

$route['preorder'] = 'preorder_c/index';
$route['preorder/(:num)'] = 'preorder_c/show/$1';
$route['preorder/new'] = 'preorder_c/add';
$route['preorder/edit/(:num)'] = 'preorder_c/edit/$1';
$route['preorder/search'] = 'preorder_c/searchPreorder';
$route['preorder/getPreOrder'] = 'preorder_c/getPreOrder';

$route['preorder/getItens'] = 'preorder_c/getItens';
$route['preorder/getClients'] = 'preorder_c/getClients';

$route['purchase_orders'] = 'purchase_Order_c/index';
$route['purchase_orders/(:num)'] = 'purchase_Order_c/show/$1';
$route['purchase_orders/new'] = 'purchase_Order_c/add';
$route['purchase_orders/edit/(:num)'] = 'purchase_Order_c/edit/$1';

$route['finances'] = 'finance_c/index';
$route['finances/(:num)'] = 'finance_c/show/$1';
$route['finances/new'] = 'finance_c/add';
$route['finances/edit/(:num)'] = 'finance_c/edit/$1';

$route['drivers'] = 'driver_c/index';
$route['drivers/(:num)'] = 'driver_c/show/$1';
$route['drivers/new'] = 'driver_c/add';
$route['drivers/edit/(:num)'] = 'driver_c/edit/$1';
$route['drivers/search'] = 'driver_c/search';