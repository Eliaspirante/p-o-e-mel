<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Resumo de Operações</h4>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="widget-panel widget-style-2 bg-white">
                        <i class="md md-attach-money text-primary"></i>
                        <h2 class="m-0 text-dark counter font-600">0</h2>
                        <div class="text-muted m-t-5">Total a receber</div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget-panel widget-style-2 bg-white">
                        <i class="md md-attach-money text-danger"></i>
                        <h2 class="m-0 text-dark counter font-600">0</h2>
                        <div class="text-muted m-t-5">Total de despesas</div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget-panel widget-style-2 bg-white">
                        <i class="md md-add-shopping-cart text-success"></i>
                        <h2 class="m-0 text-dark counter font-600"><?php echo $orders_count; ?></h2>
                        <div class="text-muted m-t-5">Vendas Ontem</div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget-panel widget-style-2 bg-white">
                        <i class="md md-add-shopping-cart text-success"></i>
                        <h2 class="m-0 text-dark counter font-600"><?php echo $orders_count_today; ?></h2>
                        <div class="text-muted m-t-5">Vendas Hoje</div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-5">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-20 header-title"><b>Financeiro hoje</b></h4>

                        <div class="nicescroll mx-box" tabindex="5001" style="overflow: hidden; outline: none;">
                            <ul class="list-unstyled transaction-list m-r-5">
                                <?php if(isset($bills) && $bills != null): ?>
                                    <?php foreach($bills as $bill): ?>
                                        <li>
                                            <i class="ti-download text-success"></i>
                                            <a href="#" title="Conta a Receber"><span class="tran-text"><?php echo $bill->name;?></span></a>
                                            <span class="pull-right text-success tran-price">+ R$ <?php echo $bill->value;?></span>
                                            <span class="pull-right text-muted"><?php echo date("d/m/Y", strtotime($bill->payday)); ?></span>
                                            <span class="clearfix"></span>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
<!--                                <li>-->
<!--                                    <i class="ti-download text-success"></i>-->
<!--                                    <a href="#" title="Conta a Receber"><span class="tran-text">Pagamento Cliente</span></a>-->
<!--                                    <span class="pull-right text-success tran-price">+$230</span>-->
<!--                                    <span class="pull-right text-muted">--><?php //echo date("d/m/Y"); ?><!--</span>-->
<!--                                    <span class="clearfix"></span>-->
<!--                                </li>-->
<!---->
<!--                                <li>-->
<!--                                    <i class="ti-upload text-danger"></i>-->
<!--                                    <a href="#" title="Conta a Pagar"><span class="tran-text">Compra de matéria prima</span></a>-->
<!--                                    <span class="pull-right text-danger tran-price">-$965</span>-->
<!--                                    <span class="pull-right text-muted">--><?php //echo date("d/m/Y"); ?><!--</span>-->
<!--                                    <span class="clearfix"></span>-->
<!--                                </li>-->

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-20 header-title"><b>Tarefas a Fazer</b></h4>
                        <div class="todoapp">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4 id="todo-message"><span id="todo-remaining">1</span> de <span id="todo-total">1</span> restando</h4>
                                </div>
<!--                                <div class="col-sm-6">-->
<!--                                    <a href="" class="pull-right btn btn-primary btn-sm waves-effect waves-light" id="btn-archive">Arquivar</a>-->
<!--                                </div>-->
                            </div>

                            <ul class="list-group no-margn nicescroll todo-list" style="height: 280px; overflow: hidden; outline: none;" id="todo-list" tabindex="5003">
                                <li class="list-group-item">
                                    <div class="checkbox checkbox-primary">
                                        <input class="todo-done" id="6" type="checkbox" checked=""><label for="6">Verificar pedidos</label>
                                    </div>
                                </li>
                            </ul>

                            <form name="todo-form" id="todo-form" role="form" class="m-t-20">
                                <div class="row">
                                    <div class="col-sm-9 todo-inputbar">
                                        <input type="text" id="todo-input-text" name="todo-input-text" class="form-control" placeholder="Adicionar nova tarefa">
                                    </div>
                                    <div class="col-sm-3 todo-send">
                                        <button class="btn-primary btn-md btn-block btn waves-effect waves-light" type="button" id="todo-btn-submit">Adcionar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->

    <footer class="footer">
        2015 © Pao e Mel.
    </footer>

</div>
