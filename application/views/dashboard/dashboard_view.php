<?php
$this->load->view("templates/header");
?>
<div class="animationload">
    <div class="loader"></div>
</div>

<!-- Begin page -->
<div id="wrapper">
    <?php
    $this->load->view("dashboard/container_dashboard");
    ?>
</div>

<?php
$this->load->view("templates/footer_scripts");
$this->load->view("templates/footer");
?>

<script src="<?php echo base_url(); ?>assets/pages/jquery.todo.js"></script>
