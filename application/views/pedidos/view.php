<div class="col-sm-12">
    <h4 class="page-title">Visualização de Pedido</h4>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="col-sm-6">
                <form id="formorder" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Descrição</label>
                        <div class="col-md-10">
                            <input disabled type="text" class="form-control" id="name_order" name="name_order" placeholder="Descrição do pré-pedido" value="<?php echo $order->name;?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="client_name">Cliente</label>
                        <div class="col-md-9">
                            <input disabled type="text" id="client_name" name="client_name" class="form-control" placeholder="Nome do Cliente" value="<?php echo $order->client_name;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Valor do pedido (R$)</label>
                        <div class="col-md-10">
                            <input disabled type="text" class="form-control" id="order_value" name="order_value" placeholder="" value="<?php echo number_format($order->value,2,",",".");?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Período do pedido</label>
                        <div class="col-md-10">
                            <?php
                            switch($order->period){
                                case 1:?>
                                    <label class="col-md-2 control-label">Manhã</label>
                                    <?php
                                    break;
                                case 2:?>
                                    <label class="col-md-2 control-label">Tarde</label>
                                    <?php
                                    break;
                                case 3:?>
                                    <label class="col-md-2 control-label">Manhã/Tarde</label>
                                    <?php
                                    break;
                            }
                            ?>
                        </div>
                    </div>

                    <p class="row">
                    <table class="table table-striped m-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Descrição Produto</th>
                            <th>Quantidade</th>
                            <th>Observação</th>
                            <th>Valor unitário</th>
                            <th>Valor total</th>
                        </tr>
                        </thead>
                        <tbody id="bodyproducts">
                        <?php if(isset($order->itens) && $order->itens != null):?>
                            <?php foreach($order->itens as $item): ?>
                                <tr>
                                    <th><?php echo $item->id; ?></th>
                                    <th><?php echo $item->description; ?></th>
                                    <th><?php echo $item->quantity; ?></th>
                                    <th><?php echo $item->observation_item; ?></th>
                                    <th><?php echo $item->value; ?></th>
                                    <th>R$ <?php echo number_format(($item->value * $item->quantity),"2",",","."); ?></th>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif;?>
                        </tbody>
                    </table>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>