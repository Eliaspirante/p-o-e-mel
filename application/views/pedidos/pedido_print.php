<html>
    <head>
        <title></title>
        <style>
            /*.center {*/
                /*margin: auto;*/
                /*!*width: 60%;*!*/
                /*text-align: left;*/
                /*!*border:3px solid #8AC007;*!*/
                /*padding: 1px;*/
            /*}*/

            IMG.displayed {
                width: 200px;
                height: 60px;
            }

            body{
                font-family: "Arial Black", arial-black;
                font-size: 0.9em;
            }
        </style>
    </head>

    <body>
        <div class="center">

            <IMG  class="displayed" src="<?php echo base_url(); ?>assets/images/logo_pao_mel.JPG" />

            Pedido: <?php echo $order->name; ?><br/>
            Número: <?php echo $order->id; ?><br/>
            Data: <?php echo date('d/m/Y', strtotime($order->created_at)); ?><br/>
            Endereço entrega: <?php echo $order->order_address; ?><br/>
            <?php if(isset($order->itens) && $order->itens != null): ?>
            <?php foreach($order->itens as $item): ?>
                    Item: <small><?php echo $item->description; ?> - <?php echo $item->quantity; ?><br/></small>
            <?php endforeach; ?>
            <?php endif; ?>

    </body>
</html>
