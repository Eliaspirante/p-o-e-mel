<div class="col-sm-12">
    <h4 class="page-title">Adição de Pré-Pedidos</h4>
</div>


<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="col-sm-6">
                <form id="formorder" action="<?php echo base_url(); ?>index.php/preorder/new" method="post" class="form-horizontal">
                    <input type="hidden" id="id_client" name="id_client" />
                    <input type="hidden" id="id_driver" name="id_driver" />
                    <div class="form-group">
                        <label class="col-md-2 control-label">Descrição</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="name_order" name="name_order" placeholder="Descrição do pré-pedido" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="client_name">Cliente</label>
                        <div class="col-md-9">
                            <input readonly type="text" id="client_name" name="client_name" class="form-control" placeholder="Nome do Cliente">
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-icon waves-effect waves-light btn-primary" data-toggle="modal" data-target="#modal-clients"> <i class="glyphicon glyphicon-search"></i> </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="driver_name">Motorista</label>
                        <div class="col-md-9">
                            <input readonly type="text" id="driver_name" name="driver_name" class="form-control" placeholder="Nome do Motorista">
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-icon waves-effect waves-light btn-primary" data-toggle="modal" data-target="#modal-drivers"> <i class="glyphicon glyphicon-search"></i> </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Valor do pedido</label>
                        <div class="col-md-10">
                            <input disabled type="text" class="form-control" id="order_value" name="order_value" placeholder="" >
                            <input type="hidden" class="form-control" id="real_value" name="real_value" placeholder="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Forma de pagamento</label>
                        <div class="col-md-10">
                            <select class="form-control" id="payment_type" name="payment_type">
                                <option value="dinheiro">Dinheiro</option>
                                <option value="cheque">Cheque</option>
                                <option value="crediario">Crediário</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Local de entrega</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="order_address" name="order_address" placeholder="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Período do pedido</label>
                        <div class="col-md-10">
                            <div class="form-group"><label class="col-md-2 control-label">Manhã</label><div class="col-md-10"><input type="radio" class="form-control" id="preorder_period" name="preorder_period" value="1" checked></div></div>
                            <div class="form-group"><label class="col-md-2 control-label">Tarde</label><div class="col-md-10"><input type="radio" class="form-control" id="preorder_period" name="preorder_period" value="2" ></div></div>
                            <div class="form-group"><label class="col-md-2 control-label">Manhã/Tarde</label><div class="col-md-10"><input type="radio" class="form-control" id="preorder_period" name="preorder_period" value="3" ></div></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Horário de Produção</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="production_time" name="production_time" placeholder="" >
                        </div>

                        <label class="col-md-3 control-label">Horário aproximado de entrega</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="delivery_time" name="delivery_time" placeholder="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <button type="button" class="btn btn-icon waves-effect waves-light btn-primary" data-toggle="modal" data-target="#modal-products">Adicionar novo produto </button>
                        </div>
                    </div>

                    <p class="row">
                        <table class="table table-striped m-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição Produto</th>
                                <th>Quantidade</th>
                                <th>Observação</th>
                                <th>Valor unitário</th>
                                <th>Valor total</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody id="bodyproducts">

                            </tbody>
                        </table>
                    </p>
                    <div id="hid_input">
                    </div>
                    <button type="submit" class="btn btn-default waves-effect waves-light btn-md">
                        Salvar
                    </button>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Clients -->
    <div id="modal-clients" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Busca de Clientes</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="query_client" name="query_client" placeholder="Digite sua busca" />
                        </div>
                        <div class="col-md-2">
                            <button id="btnBuscaClientes" onclick="consultClientes()" type="button" class="btn btn-icon waves-effect waves-light btn-primary">Buscar</button>
                        </div>
                    </div>
                    <p class="row">
                        <table class="table table-striped m-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome Cliente</th>
                            </tr>
                            </thead>
                            <tbody id="bodyclientsearch">

                            </tbody>
                        </table>
                    </p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- End Modal Clients -->
    <!-- Modal Products -->
    <div id="modal-products" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Busca de Produtos</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="query_product" name="query_product" placeholder="Digite sua busca" />
                        </div>
                        <div class="col-md-2">
                            <button id="btnBuscaItens" onclick="consultItens()" type="button" class="btn btn-icon waves-effect waves-light btn-primary">Buscar</button>
                        </div>
                    </div>
                    <p class="row">
                        <table class="table table-striped m-0">
                            <thead>
                            <tr>
                                <th>Código</th>
                                <th>Código EAN</th>
                                <th>Descrição Produto</th>
                                <th>Valor unitário</th>
                            </tr>
                            </thead>
                            <tbody id="bodyproductsearch">

                            </tbody>
                        </table>
                    </p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- End Modal Products -->
    <!-- Modal Drivers -->
    <div id="modal-drivers" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Busca de Motoristas</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="query_driver" name="query_driver" placeholder="Digite sua busca" />
                        </div>
                        <div class="col-md-2">
                            <button id="btnBuscaMotoristas" onclick="consultDrivers()" type="button" class="btn btn-icon waves-effect waves-light btn-primary">Buscar</button>
                        </div>
                    </div>
                    <p class="row">
                        <table class="table table-striped m-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome Motorista</th>
                            </tr>
                            </thead>
                            <tbody id="bodydriversearch">

                            </tbody>
                        </table>
                    </p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- End Modal Drivers -->
</div>

<!-- Modal-Effect -->
<script src="<?php echo base_url();?>assets/plugins/custombox/dist/custombox.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/custombox/dist/legacy.min.js"></script>
<script id="hiddenlpsubmitdiv" style="display: none;"></script>

<script>

    function consultClientes(){
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>index.php/preorder/getClients",
            data: { query: $("#query_client").val() }
        }).done(function( msg ) {
            document.getElementById("bodyclientsearch").innerHTML = "";

            var clients = JSON.parse(msg);

            for (i = 0; i < clients.length; i++) {
                var client = clients[i];

                var linha ="<tr>" +
                    "<td>"+client.id+"</td>"+
                    "<td>"+client.nome+"</td>"+
                    "<td>"+
                    '<a href="#" onclick="selectClient('+client.id+', \''+client.nome+'\')" class="btn btn-icon waves-effect waves-light btn-success btn-sm" title="Selecionar"><i class="glyphicon glyphicon-ok"></i> </a>' +
                    "</td>"+
                    "</tr>";

                document.getElementById("bodyclientsearch").innerHTML += linha;
            }
        });
    }

    function selectClient(id, nome){
        $('input[name="id_client"').val(id);
        $('input[name="client_name"').val(nome);

        $('#modal-clients').modal('hide');
    }

    function consultDrivers(){
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>index.php/drivers/search",
            data: { query: $("#query_driver").val() }
        }).done(function( msg ) {
            document.getElementById("bodydriversearch").innerHTML = "";

            var drivers = JSON.parse(msg);

            for (i = 0; i < drivers.length; i++) {
                var driver = drivers[i];

                var linha ="<tr>" +
                    "<td>"+driver.id+"</td>"+
                    "<td>"+driver.name+"</td>"+
                    "<td>"+
                    '<a href="#" onclick="selectDriver('+driver.id+', \''+driver.name+'\')" class="btn btn-icon waves-effect waves-light btn-success btn-sm" title="Selecionar"><i class="glyphicon glyphicon-ok"></i> </a>' +
                    "</td>"+
                    "</tr>";

                document.getElementById("bodydriversearch").innerHTML += linha;
            }
        });
    }

    function selectDriver(id, nome){
        $('input[name="id_driver"').val(id);
        $('input[name="driver_name"').val(nome);

        $('#modal-drivers').modal('hide');
    }

    function consultItens(){
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>index.php/preorder/getItens",
            data: { query: $("#query_product").val(),
                    id_cliente: $('input[name="id_client"').val()
            }
        }).done(function( msg ) {
            document.getElementById("bodyproductsearch").innerHTML = "";

            var itens = JSON.parse(msg);

            for (i = 0; i < itens.length; i++) {
                var item = itens[i];

                var linha ="<tr>" +
                    "<td>"+item.id+"</td>"+
                    "<td>"+item.ean+"</td>"+
                    "<td>"+item.descricao+"</td>"+
                    "<td>"+formatReal(item.preco)+"</td>"+
                    "<td>"+
                        '<a href="#" onclick="selectItem('+item.id+', \''+item.descricao+'\',\''+item.preco+'\')" class="btn btn-icon waves-effect waves-light btn-success btn-sm" title="Selecionar"><i class="glyphicon glyphicon-ok"></i> </a>' +
                    "</td>"+
                    "</tr>";

                document.getElementById("bodyproductsearch").innerHTML += linha;
            }
        });
    }

    function selectItem(id, descricao, preco){

        var linha = "<tr id='line_"+id+"'>" +
            "<td>"+id+"</td>"+
            "<td>"+descricao+"</td>"+
            "<td>"+
                '<input type="text" value="1" onblur="calculate(this,'+id+','+preco+')" id="quantity_expected[]" name="quantity_expected[]"/>' +
            "</td>"+
            "<td>"+
                '<input type="text" value="" id="observation_item[]" name="observation_item[]"/>' +
            "</td>"+
            "<td>"+
                formatReal(preco) +
            "</td>"+
            "<td id='total_"+id+"'>"+formatReal(preco)+"</td>"+
            "<td>"+
                '<a href="#" onclick="removeItem('+id+')" class="btn btn-icon waves-effect waves-light btn-danger btn-sm" title="Remover"><i class="fa fa-close"></i> </a>' +
            "</td>"+
                '<input type="hidden" value="'+preco+'" id="single_value[]" name="single_value[]"/>' +
                '<input type="hidden" value="'+id+'" id="id_item[]" name="id_item[]"/>' +
                '<input type="hidden" value="'+descricao+'" id="item_description[]" name="item_description[]"/>' +
            "</tr>";

        var quantity = document.getElementsByName('quantity_expected[]');
        var observations = document.getElementsByName('observation_item[]');

        var quantity_temp = new Array();
        var observation_temp = new Array();

        for (var i = 0; i < quantity.length; i++) {
            quantity_temp.push(quantity.item(i).value);
        }

        for (var i = 0; i < observations.length; i++) {
            observation_temp.push(observations.item(i).value);
        }

        document.getElementById("bodyproducts").innerHTML += linha;

        for (var i = 0; i < quantity_temp.length; i++) {
            quantity.item(i).value = quantity_temp[i];
        }

        for (var i = 0; i < observation_temp.length; i++) {
            observations.item(i).value = observation_temp[i];
        }

        calculateAll();

        $('#modal-products').modal('hide');

    }

    function removeItem(id){
        $("#line_"+id).remove();
    }

    function calculate(item, id, preco){
        var total_calc = parseFloat(item.value) * parseFloat(preco);

        document.getElementById('total_'+id).innerHTML = formatReal(parseFloat(total_calc))+"";

        calculateAll();
    }

    function calculateAll(){
        document.getElementsByName('quantity_expected[]');

        var quantity = document.getElementsByName('quantity_expected[]');
        var values = document.getElementsByName('single_value[]');

        var total = parseFloat(0);

        for (var i = 0; i < quantity.length; i++) {
            total = total + parseFloat(quantity.item(i).value) * parseFloat(values.item(i).value);
        }

        document.getElementById('order_value').value = formatReal(total);
        document.getElementById('real_value').value = total;
    }

    function formatReal(n) {
        try {
            return "R$ " + n.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
        }catch (err){
            return "R$ " + n.replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
        }
    }
</script>