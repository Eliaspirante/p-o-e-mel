<div class="col-sm-12">
    <h4 class="page-title">Lista de Pré-Pedidos</h4>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="row">
                <div class="col-sm-6 text-xs-center">
                    <div class="form-group">
                        <a href="<?php echo base_url();?>index.php/preorder/new" class="btn btn-default m-b-20"><i class="fa fa-plus m-r-5"></i> Adicionar novo Pré-Pedido</a>
                    </div>
                </div>
            </div>
            <?php if(!isset($pedidos) || $pedidos == null || count($pedidos) < 0): ?>
                Nenhum pré-pedido cadastrado!
            <?php else: ?>
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Descrição</th>
                        <th>Cliente</th>
                        <th>Quantidade Itens</th>
                        <th>Valor Total (R$)</th>
                        <th>Criado em</th>
                        <th>Alterado em</th>
                        <th>Açoes</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php if(isset($pedidos) && $pedidos != null): ?>
                        <?php foreach($pedidos as $pedido): ?>
                            <tr>
                                <td><a href="<?php echo base_url();?>index.php/preorder/<?php echo $pedido->id; ?>"><?php echo $pedido->id; ?></a></td>
                                <td><?php echo $pedido->name; ?></td>
                                <td><?php echo $pedido->client_name; ?></td>
                                <td><?php if(isset($pedido->itens))echo count($pedido->itens); else echo ""; ?></td>
                                <td><?php echo $pedido->value; ?></td>
                                <td><?php echo date('d/m/Y',strtotime($pedido->created_at)); ?></td>
                                <td><?php if(isset($pedido->updated_at) && $pedido->updated_at != '0000-00-00 00:00:00')echo date('d/m/Y',strtotime($pedido->updated_at)); ?></td>
                                <td>
                                    <a href="#" class="btn btn-icon waves-effect waves-light btn-success btn-sm" title="Realizar Venda"><i class="fa fa-money"></i> </a>
                                    <a href="<?php echo base_url();?>index.php/preorder/edit/<?php echo $pedido->id; ?>" class="btn btn-icon waves-effect waves-light btn-warning btn-sm" title="Editar"><i class="fa fa-pencil"></i> </a>
                                    <a href="#" class="btn btn-icon waves-effect waves-light btn-danger btn-sm" title="Apagar"><i class="fa fa-remove"></i> </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
    } );
</script>


