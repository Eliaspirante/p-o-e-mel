<?php
$this->load->view("templates/header");
?>
    <div class="animationload">
        <div class="loader"></div>
    </div>

    <!-- Begin page -->
    <div id="wrapper">
        <?php
        $this->load->view("compras/container_purchase_order");
        ?>
    </div>

<?php
$this->load->view("templates/footer");
?>