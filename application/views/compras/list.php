<div class="col-sm-12">
    <h4 class="page-title">Lista de Compras</h4>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="row">
                <div class="col-sm-6 text-xs-center">
                    <div class="form-group">
                        <a href="<?php echo base_url();?>index.php/purchase_orders/new" class="btn btn-default m-b-20"><i class="fa fa-plus m-r-5"></i> Adicionar nova Compra</a>
                    </div>
                </div>
            </div>
            <?php if(!isset($compras) || $compras == null || count($compras) < 0): ?>
                Nenhuma compra cadastrado!
            <?php else: ?>
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Descrição</th>
                        <th>Quantidade Itens</th>
                        <th>Valor Total (R$)</th>
                        <th>Criado em</th>
                        <th>Alterado em</th>
                        <th>Açoes</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php if(isset($compras) && $compras != null): ?>
                        <?php foreach($compras as $compra): ?>
                            <tr>
                                <td><a href="<?php echo base_url();?>index.php/purchase_orders/<?php echo $compra->id; ?>"><?php echo $compra->id; ?></a></td>
                                <td><?php echo $compra->name; ?></td>
                                <td><?php if(isset($compra->itens))echo count($compra->itens); else echo ""; ?></td>
                                <td><?php echo $compra->value; ?></td>
                                <td><?php echo date('d/m/Y',strtotime($compra->created_at)); ?></td>
                                <td><?php if(isset($compra->updated_at) && $compra->updated_at != '0000-00-00 00:00:00')echo date('d/m/Y',strtotime($compra->updated_at)); ?></td>
                                <td>
                                    <a href="<?php echo base_url();?>index.php/purchase_orders/edit/<?php echo $compra->id; ?>" class="btn btn-icon waves-effect waves-light btn-warning btn-sm" title="Editar"><i class="fa fa-pencil"></i> </a>
                                    <a href="#" class="btn btn-icon waves-effect waves-light btn-danger btn-sm" title="Apagar"><i class="fa fa-remove"></i> </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
    } );
</script>


