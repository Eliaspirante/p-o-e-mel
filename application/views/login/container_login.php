<div class="animationload">
    <div class="loader"></div>
</div>

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
            <h3 class="text-center"> Entrar no sistema <strong class="text-custom">Pão e Mel</strong> </h3>
        </div>


        <div class="panel-body">
            <form class="form-horizontal m-t-20" action="<?php echo base_url();?>index.php/login" method="POST">

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input name="user" id="user" class="form-control" type="text" required="" placeholder="Usuário">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input name="pass" id="pass" class="form-control" type="password" required="" placeholder="Senha">
                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-block text-uppercase waves-effect waves-light" type="submit">Entrar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>
