<?php
$this->load->view("templates/header");
?>
    <div class="animationload">
        <div class="loader"></div>
    </div>

    <!-- Begin page -->
    <div id="wrapper">
        <?php
        $this->load->view("financeiro/container_finances");
        ?>
    </div>

<?php
$this->load->view("templates/footer");
?>