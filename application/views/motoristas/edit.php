<div class="col-sm-12">
    <h4 class="page-title">Edição de Motorista</h4>
</div>


<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="col-sm-6">
                <form id="formorder" action="<?php echo base_url(); ?>index.php/drivers/edit/<?php echo $motorista->id; ?>" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Nome:</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nome do motorista" value="<?php echo $motorista->name; ?>">
                        </div>
                    </div>
                    </p>
                    <button type="submit" class="btn btn-default waves-effect waves-light btn-md">
                        Salvar
                    </button>
                </form>
            </div>
        </div>
    </div>

</div>

<!-- Modal-Effect -->
<script src="<?php echo base_url();?>assets/plugins/custombox/dist/custombox.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/custombox/dist/legacy.min.js"></script>
<script id="hiddenlpsubmitdiv" style="display: none;"></script>