<!-- Top Bar Start -->
<?php $this->load->view("templates/top_bar_dashboard"); ?>
<!-- Top Bar End -->

<!-- ========== Left Sidebar Start ========== -->
<?php $this->load->view("templates/left_sidebar_dashboard"); ?>
<!-- Left Sidebar End -->

<?php $this->load->view("templates/footer_scripts"); ?>

<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <?php $this->load->view($view); ?>
        </div> <!-- container -->
    </div> <!-- content -->

    <footer class="footer">
        2015 © Pao e Mel.
    </footer>

</div>
<!-- Left Sidebar End -->