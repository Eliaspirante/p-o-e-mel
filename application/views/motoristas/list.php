<div class="col-sm-12">
    <h4 class="page-title">Lista de Motoristas</h4>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="row">
                <div class="col-sm-6 text-xs-center">
                    <div class="form-group">
                        <a href="<?php echo base_url();?>index.php/drivers/new" class="btn btn-default m-b-20"><i class="fa fa-plus m-r-5"></i> Adicionar novo Motorista</a>
                    </div>
                </div>
            </div>
            <?php if(!isset($motoristas) || $motoristas == null || count($motoristas) < 0): ?>
                Nenhum motorista cadastrado!
            <?php else: ?>
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nome</th>
                        <th>Criado em</th>
                        <th>Alterado em</th>
                        <th>Açoes</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php if(isset($motoristas) && $motoristas != null): ?>
                        <?php foreach($motoristas as $motorista): ?>
                            <tr>
                                <td><a href="<?php echo base_url();?>index.php/drivers/<?php echo $motorista->id; ?>"><?php echo $motorista->id; ?></a></td>
                                <td><?php echo $motorista->name; ?></td>
                                <td><?php echo date('d/m/Y',strtotime($motorista->created_at)); ?></td>
                                <td><?php if(isset($motorista->updated_at) && $motorista->updated_at != '0000-00-00 00:00:00')echo date('d/m/Y',strtotime($motorista->updated_at)); ?></td>
                                <td>
                                    <a href="<?php echo base_url();?>index.php/drivers/edit/<?php echo $motorista->id; ?>" class="btn btn-icon waves-effect waves-light btn-warning btn-sm" title="Editar"><i class="fa fa-pencil"></i> </a>
                                    <a href="#" class="btn btn-icon waves-effect waves-light btn-danger btn-sm" title="Apagar"><i class="fa fa-remove"></i> </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
    } );
</script>


