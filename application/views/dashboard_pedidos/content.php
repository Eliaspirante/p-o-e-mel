<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Pedidos para produção</h4>
                </div>
            </div>
            <br/>
            <div class="row">
                <?php if(isset($orders) && $orders != null): ?>
                    <?php foreach($orders as $order): ?>
                        <div class="col-lg-4">
                            <div class="card-box">
                                <h4 class="m-t-0 m-b-20 header-title"><b>Pedido - <?php echo $order->id. " - ". $order->name; ?></b></h4>

                                <div class="inbox-widget nicescroll mx-box" tabindex="5001" style="overflow: hidden; outline: none;">
                                    <ul class="list-group no-margn nicescroll todo-list" style="height: 280px; overflow: hidden; outline: none;" id="todo-list" tabindex="5003">
                                        <?php if(isset($order->itens) && $order->itens != null): ?>
                                            <?php foreach($order->itens as $item): ?>
                                                <li class="list-group-item">
                                                    <div class="checkbox checkbox-primary">
                                                        <input class="todo-done" id="item<?php echo $item->id; ?>" type="checkbox" onchange="updateItem(<?php echo $item->id;?>)" />
                                                        <label for="item<?php echo $item->id; ?>"><?php echo $item->description; ?> - Quantidade: <?php echo $item->quantity; ?></label>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="row">
                                    <button id="btn<?php echo $order->id; ?>" class="btn-primary btn-md btn-block btn waves-effect waves-light" type="button" id="todo-btn-submit" onclick="printOrder('<?php echo $order->id; ?>')">Concluir</button>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h4>Não existem pedidos a serem produzidos!</h4>
                <?php endif;?>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->

    <footer class="footer">
        2015 © Pao e Mel.
    </footer>

</div>

<script>
    function updateItem(id){
        $.ajax({
            method: "POST",
            url: "<?php echo base_url();?>index.php/updateitemorder",
            data: { id_item: id,
                status: $("#chck"+id).is(":checked")
            }
        }).done(function( msg ) {
            if(msg == undefined)
                return;

            msg = JSON.parse(msg);

            if(msg.code == 200){

            }
            else{
                $.bootstrapGrowl('<h4>Erro!</h4> <p>'+ msg.error +'</p>', {
                    type: "danger",
                    delay: 3500,
                    allow_dismiss: true
                });
            }

        });
    }

    function printOrder(id_order) {
        window.open('<?php echo base_url();?>index.php/printorder/' + id_order, '_blank', 'Recibo', 'width=80, height=20, toolbar=no,scrollbars=no,resizable=no');
    }
</script>