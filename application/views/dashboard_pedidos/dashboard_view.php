<?php
$this->load->view("templates/header");
?>
<div class="animationload">
    <div class="loader"></div>
</div>

<!-- Begin page -->
<div id="wrapper">
    <?php
    $this->load->view("dashboard_pedidos/container_dashboard");
    ?>
</div>

<?php
$this->load->view("templates/footer_scripts");
$this->load->view("templates/footer");
?>
