<div class="col-sm-12">
    <h4 class="page-title">Lista de Contas a Receber</h4>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="row">
                <div class="col-sm-6 text-xs-center">
                    <div class="form-group">
                        <a href="<?php echo base_url();?>index.php/finances/new" class="btn btn-default m-b-20"><i class="fa fa-plus m-r-5"></i> Adicionar nova conta a Receber</a>
                    </div>
                </div>
            </div>
            <?php if(!isset($contas) || $contas == null || count($contas) < 0): ?>
                Nenhum conta cadastra!
            <?php else: ?>
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Descrição</th>
                        <th>Cliente</th>
                        <th>Data Vencimento</th>
                        <th>Data Pagamento</th>
                        <th>Valor Total (R$)</th>
                        <th>Criado em</th>
                        <th>Alterado em</th>
                        <th>Açoes</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php if(isset($contas) && $contas != null): ?>
                        <?php foreach($contas as $conta): ?>
                            <tr>
                                <td><a href="<?php echo base_url();?>index.php/finances/<?php echo $conta->id; ?>"><?php echo $conta->id; ?></a></td>
                                <td><?php echo $conta->name; ?></td>
                                <td><?php echo $conta->client_name;?></td>
                                <td><?php if(isset($conta->payday) && $conta->payday != "" && $conta->payday != '0000-00-00')echo date('d/m/Y',strtotime($conta->payday));?></td>
                                <td><?php if(isset($conta->pay_at) && $conta->pay_at != "" && $conta->pay_at != '0000-00-00')echo date('d/m/Y',strtotime($conta->pay_at));?></td>
                                <td><?php echo $conta->value; ?></td>
                                <td><?php echo date('d/m/Y',strtotime($conta->created_at)); ?></td>
                                <td><?php if(isset($compra->updated_at) && $compra->updated_at != '0000-00-00 00:00:00')echo date('d/m/Y',strtotime($conta->updated_at)); ?></td>
                                <td>
                                    <a href="<?php echo base_url();?>index.php/finances/edit/<?php echo $conta->id; ?>" class="btn btn-icon waves-effect waves-light btn-warning btn-sm" title="Editar"><i class="fa fa-pencil"></i> </a>
                                    <a href="#" class="btn btn-icon waves-effect waves-light btn-danger btn-sm" title="Apagar"><i class="fa fa-remove"></i> </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable();
    } );
</script>


