<div class="col-sm-12">
    <h4 class="page-title">Adição de Conta a Receber</h4>
</div>


<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="col-sm-6">
                <form id="formorder" action="<?php echo base_url(); ?>index.php/finances/new" method="post" class="form-horizontal">
                    <input type="hidden" id="id_client" name="id_client" />
                    <div class="form-group">
                        <label class="col-md-2 control-label">Descrição</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Descrição da conta" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="client_name">Cliente</label>
                        <div class="col-md-9">
                            <input readonly type="text" id="client_name" name="client_name" class="form-control" placeholder="Nome do Cliente">
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn btn-icon waves-effect waves-light btn-primary" data-toggle="modal" data-target="#modal-clients"> <i class="glyphicon glyphicon-search"></i> </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Valor</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="real_value" name="real_value" placeholder="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Data Vencimento</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="payday" name="payday" placeholder="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Data Pagamento</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="pay_at" name="pay_at"" placeholder="" >
                        </div>
                    </div>
                    </p>
                    <button type="submit" class="btn btn-default waves-effect waves-light btn-md">
                        Salvar
                    </button>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Clients -->
    <div id="modal-clients" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Busca de Clientes</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="query_client" name="query_client" placeholder="Digite sua busca" />
                        </div>
                        <div class="col-md-2">
                            <button id="btnBuscaClientes" onclick="consultClientes()" type="button" class="btn btn-icon waves-effect waves-light btn-primary">Buscar</button>
                        </div>
                    </div>
                    <p class="row">
                    <table class="table table-striped m-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome Cliente</th>
                        </tr>
                        </thead>
                        <tbody id="bodyclientsearch">

                        </tbody>
                    </table>
                    </p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- End Modal Clients -->

</div>

<!-- Modal-Effect -->
<script src="<?php echo base_url();?>assets/plugins/custombox/dist/custombox.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/custombox/dist/legacy.min.js"></script>
<script id="hiddenlpsubmitdiv" style="display: none;"></script>

<script>

    function consultClientes(){
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>index.php/preorder/getClients",
            data: { query: $("#query_client").val() }
        }).done(function( msg ) {
            document.getElementById("bodyclientsearch").innerHTML = "";

            var clients = JSON.parse(msg);

            for (i = 0; i < clients.length; i++) {
                var client = clients[i];

                var linha ="<tr>" +
                    "<td>"+client.id+"</td>"+
                    "<td>"+client.nome+"</td>"+
                    "<td>"+
                    '<a href="#" onclick="selectClient('+client.id+', \''+client.nome+'\')" class="btn btn-icon waves-effect waves-light btn-success btn-sm" title="Selecionar"><i class="glyphicon glyphicon-ok"></i> </a>' +
                    "</td>"+
                    "</tr>";

                document.getElementById("bodyclientsearch").innerHTML += linha;
            }
        });
    }

    function selectClient(id, nome){
        $('input[name="id_client"').val(id);
        $('input[name="client_name"').val(nome);

        $('#modal-clients').modal('hide');
    }


    function calculate(item, id, preco){
        var total_calc = parseFloat(item.value) * parseFloat(preco);

        document.getElementById('total_'+id).innerHTML = formatReal(parseFloat(total_calc))+"";

        calculateAll();
    }

    function calculateAll(){
        document.getElementsByName('quantity_expected[]');

        var quantity = document.getElementsByName('quantity_expected[]');
        var values = document.getElementsByName('single_value[]');

        var total = parseFloat(0);

        for (var i = 0; i < quantity.length; i++) {
            total = total + parseFloat(quantity.item(i).value) * parseFloat(values.item(i).value);
        }

        document.getElementById('order_value').value = formatReal(total);
        document.getElementById('real_value').value = total;
    }

    function formatReal(n) {
        try {
            return "R$ " + n.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
        }catch (err){
            return "R$ " + n.replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
        }
    }


</script>
