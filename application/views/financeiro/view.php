<div class="col-sm-12">
    <h4 class="page-title">Visualização de Conta a Receber</h4>
</div>


<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <div class="col-sm-6">
                <form id="formorder" class="form-horizontal">
                    <input type="hidden" id="id_client" name="id_client" value="<?php echo $conta->id_client; ?>"/>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Descrição</label>
                        <div class="col-md-10">
                            <input readonly type="text" class="form-control" id="name" name="name" placeholder="Descrição da conta" value="<?php echo $conta->name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="client_name">Cliente</label>
                        <div class="col-md-9">
                            <input readonly type="text" id="client_name" name="client_name" class="form-control" placeholder="Nome do Cliente" value="<?php echo $conta->client_name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Valor</label>
                        <div class="col-md-10">
                            <input readonly type="text" class="form-control" id="real_value" name="real_value" placeholder="" value="<?php echo $conta->value; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Data Vencimento</label>
                        <div class="col-md-10">
                            <input readonly type="text" class="form-control" id="payday" name="payday" placeholder="" value="<?php echo $conta->payday; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Data Pagamento</label>
                        <div class="col-md-10">
                            <input readonly type="text" class="form-control" id="pay_at" name="pay_at"" placeholder="" value="<?php echo $conta->pay_at; ?>">
                        </div>
                    </div>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal-Effect -->
<script src="<?php echo base_url();?>assets/plugins/custombox/dist/custombox.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/custombox/dist/legacy.min.js"></script>
<script id="hiddenlpsubmitdiv" style="display: none;"></script>
