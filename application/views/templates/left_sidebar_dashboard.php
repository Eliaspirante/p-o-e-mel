<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

                <li class="text-muted menu-title">Navegação</li>

                <li>
                    <a class="waves-effect" href="<?php echo base_url();?>"><i class="ti-home"></i>Principal</a>
                </li>

                <li>
                    <a class="waves-effect" href="<?php echo base_url();?>index.php/dashboard_orders"><i class="ti-home"></i>Principal Pedidos</a>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-share"></i><span>Cadastros </span></a>
                    <ul>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><span>Entregas</span> </a>
                            <ul style="">
                                <li><a href="<?php echo base_url(); ?>index.php/preorder"><span>Pré Entregas</span></a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/order"><span>Entregas</span></a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url(); ?>index.php/purchase_orders" class="waves-effect"><span>Compras</span> </a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/drivers" class="waves-effect"><span>Motoristas</span> </a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-share"></i><span>Financeiro </span></a>
                    <ul>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><span>Contas</span> </a>
                            <ul style="">
                                <li><a href="javascript:void(0);"><span>A Pagar</span></a></li>
                                <li><a href="<?php echo base_url(); ?>index.php/finances"><span>A Receber</span></a></li>
                            </ul>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><span>Relatórios</span> </a>
                            <ul style="">
                                <li><a href="javascript:void(0);"><span>A Pagar</span></a></li>
                                <li><a href="javascript:void(0);"><span>A Receber</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </li>


            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
