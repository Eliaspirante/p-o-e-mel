<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bill_model extends MY_Model
{
    public $table = 'bills';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }
}