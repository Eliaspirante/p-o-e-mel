<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orderitem_model extends MY_Model
{
    public $table = 'orders_itens';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }
}