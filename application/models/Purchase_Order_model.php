<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_Order_model extends MY_Model
{
    public $table = 'purchase_orders';
    public $primary_key = 'id';

    public function __construct()
    {
        $this->has_many['itens'] = array('Purchase_item_model','id_purchase', 'id');
        parent::__construct();
    }
}