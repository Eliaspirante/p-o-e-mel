<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends MY_Model
{
    public $table = 'orders';
    public $primary_key = 'id';

    public function __construct()
    {
        $this->has_many['itens'] = array('Orderitem_model','id_order', 'id');
        parent::__construct();
    }
}