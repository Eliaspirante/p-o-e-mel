<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver_model extends MY_Model
{
    public $table = 'users_drivers';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }
}