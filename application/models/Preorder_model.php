<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Preorder_model extends MY_Model
{
    public $table = 'preorders';
    public $primary_key = 'id';

    public function __construct()
    {
        $this->has_many['itens'] = array('Preorderitem_model','id_preorder', 'id');

        parent::__construct();
    }
}