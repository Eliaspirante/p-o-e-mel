<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_item_model extends MY_Model
{
    public $table = 'purchase_orders_itens';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }
}