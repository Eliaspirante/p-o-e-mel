<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_c extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $this->load->view('login/login_view');
    }

    public function doLogin(){
        if ($this->ion_auth->login($this->input->post('user'), $this->input->post('pass')))
        {
            //if the login is successful
            //redirect them back to the home page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect('dashboard', 'refresh');
        }
        else
        {
            //if the login was un-successful
            //redirect them back to the login page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect('/', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
        }
    }

    public function doLogout(){
        $this->ion_auth->logout();

        $this->session->unset_userdata(array('logged' => '0'));

        redirect('/', 'refresh');
    }
}