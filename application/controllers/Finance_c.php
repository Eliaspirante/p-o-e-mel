<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Finance_c extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->logged_in()){
            redirect("entrar","refresh");
        }

        $this->load->model("Bill_model");
    }

    public function index()
    {
        $data['view'] = 'financeiro/list';

        $data["contas"] = $this->Bill_model->get_all();

        $this->load->view('financeiro/container_view', $data);
    }

    public function show($id = null){
        if($id == null){
            redirect('dashboard','refresh');
        }

        $data['conta'] = $this->Bill_model->get($id);
        $data['view'] = 'financeiro/view';

        $this->load->view('financeiro/container_view', $data);
    }

    public function add(){
        if($_POST){

            $this->load->model("Bill_model");

            $bill_data = array(
                "name" => $_POST["name"],
                "client_name" => $_POST["client_name"],
                "value" => $_POST["real_value"],
                "payday" => $_POST["payday"],
                "pay_at" => $_POST["pay_at"],
                "type" => 1
            );

            $id = $this->Bill_model->insert($bill_data);

            redirect("finances/".$id,'refresh');
        }

        $data['view'] = 'financeiro/new';

        $this->load->view('financeiro/container_view', $data);
    }

    public function edit($id = null){
        if($id == null){
            redirect('dashboard','refresh');
        }

        if($_POST){
            $data_order = array(
                "name" => $_POST["name"],
                "client_name" => $_POST["client_name"],
                "value" => $_POST["real_value"],
                "payday" => $_POST["payday"],
                "pay_at" => $_POST["pay_at"],
                "type" => 1
            );

            $updated = $this->Bill_model->update($data_order, $id);


        }

        $data['conta'] = $this->Bill_model->get($id);

        $data['view'] = 'financeiro/edit';

        $this->load->view('financeiro/container_view', $data);
    }
}
