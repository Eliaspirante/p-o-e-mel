<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_c extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->logged_in()){
            redirect("entrar","refresh");
        }

        $this->load->model("Order_model");
        $this->load->model("Bill_model");
    }

    public function index()
    {
        $data["orders_count"] = $this->Order_model->with_itens()->where("created_at","<=",date('Y-m-d H:i:s', strtotime("-1 day")))->count();
        $data["orders_count_today"] = $this->Order_model->with_itens()->where("created_at",">=", date('Y-m-d H:i:s', strtotime("-1 day")))->count();
        $data["bills"] = $this->Bill_model->where("pay_at",null)->get_all();
        $this->load->view('dashboard/dashboard_view', $data);
    }

    public function orders(){

        $data["orders"] = $this->Order_model->with_itens()->get_all();

        $this->load->view('dashboard_pedidos/dashboard_view', $data);
    }
}
