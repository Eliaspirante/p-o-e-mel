<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_c extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->logged_in()){
            redirect("entrar","refresh");
        }

        $this->load->model("Order_model");
        $this->load->model("Orderitem_model");
    }

    public function index()
    {
        $data['view'] = 'pedidos/list';

        $data["pedidos"] = $this->Order_model->with_itens()->get_all();

        $this->load->view('pedidos/container_view', $data);
    }

    public function show($id = null){
        if($id == null){
            redirect('dashboard','refresh');
        }

        $data['order'] = $this->Order_model->with_itens()->get($id);
        $data['view'] = 'pedidos/view';

        $this->load->view('pedidos/container_view', $data);
    }

    public function add(){
        if($_POST){
            $data_order = array(
                "name" => $_POST["name_order"],
                "period" => $_POST["order_period"],
                "id_client" => $_POST["id_client"],
                "id_driver" => $_POST["id_driver"],
                "driver_name" => $_POST["driver_name"],
                "client_name" => $_POST["client_name"],
                "production_time" => $_POST["production_time"],
                "delivery_time" => $_POST["delivery_time"],
                "payment_type" => $_POST["payment_type"],
                "value" => $_POST["real_value"]
            );

            $id = $this->Order_model->insert($data_order);

            if($id){
                for($i= 0; $i < count($_POST["id_item"]); $i++) {
                    $data_order_item = array(
                        "id_order" => $id,
                        "quantity" =>$_POST["quantity_expected"][$i],
                        "value" =>$_POST["single_value"][$i],
                        "id_item" =>$_POST["id_item"][$i],
                        "description" =>$_POST["item_description"][$i],
                        "observation_item" =>$_POST["observation_item"][$i]
                    );

                    $id_item = $this->Orderitem_model->insert($data_order_item);
                }
            }

            $this->load->model("Bill_model");

            $bill_data = array(
                "id_order" => $id,
                "id_client" => $_POST["id_client"],
                "name" => "Pedido - ".$id,
                "client_name" => $_POST["client_name"],
                "value" => $_POST["real_value"],
                "payday" => date("Y-m-d", strtotime("+1 day")),
                "type" => 1
            );

            $this->Bill_model->insert($bill_data);

            redirect("order/".$id,'refresh');
        }

        $data['view'] = 'pedidos/new';

        $this->load->view('pedidos/container_view', $data);
    }

    public function edit($id = null){
        if($id == null){
            redirect('dashboard','refresh');
        }

        if($_POST){
            $data_order = array(
                "name" => $_POST["name_order"],
                "period" => $_POST["order_period"],
                "id_client" => $_POST["id_client"],
                "id_driver" => $_POST["id_driver"],
                "driver_name" => $_POST["driver_name"],
                "client_name" => $_POST["client_name"],
                "production_time" => $_POST["production_time"],
                "delivery_time" => $_POST["delivery_time"],
                "payment_type" => $_POST["payment_type"],
                "value" => $_POST["real_value"]
            );

            $updated = $this->Order_model->update($data_order, $id);

            if($updated){
                if(array_key_exists("id_item",$_POST)) {
                    for ($i = 0; $i < count($_POST["id_item"]); $i++) {
                        $data_order_item = array(
                            "id_order" => $id,
                            "quantity" => $_POST["quantity_expected"][$i],
                            "value" => $_POST["single_value"][$i],
                            "id_item" => $_POST["id_item"][$i],
                            "description" =>$_POST["item_description"][$i],
                            "observation_item" =>$_POST["observation_item"][$i]
                        );

                        $id_item = $this->Orderitem_model->insert($data_order_item);
                    }
                }

                foreach($_POST as $key => $value){
                    if(strpos($key, "itemexclusion_") !== false){
                        $id_to_delete = explode("_", $key);

                        $file_to_delete = $this->Orderitem_model->get($id_to_delete[1]);

                        if($file_to_delete != false){
                            $this->Orderitem_model->delete($id_to_delete[1]);
                        }
                    }
                }

                $this->load->model("Bill_model");

                $bill_data = array(
                    "id_order" => $id,
                    "id_client" => $_POST["id_client"],
                    "name" => "Pedido - ".$id,
                    "client_name" => $_POST["client_name"],
                    "value" => $_POST["real_value"],
                    "type" => 1
                );

                $this->Bill_model->update($bill_data, $id);
            }
        }

        $data['order'] = $this->Order_model->with_itens()->get($id);

        $data['view'] = 'pedidos/edit';

        $this->load->view('pedidos/container_view', $data);
    }

    public function closeOrder(){
        if($_POST){
            $updated = $this->Order_model->update(array("active" => "1"),$_POST['id_order']);

            if($updated){
                $message = array("code" => 200, "message" => "OK");
            }else{
                $message = array("code" => 500, "message" => "Erro");
            }

            echo json_encode($message);
        }
    }

    public function updateItemOrder(){
        if($_POST){
            $pronto = 0;

            if($_POST['status'] == 'checked')
                $pronto = 1;

            $updated = $this->Orderitem_model->update(array("peso" => $_POST[""],"done" => $pronto),$_POST['id_item']);

            if($updated){
                $message = array("code" => 200, "message" => "OK");
            }else{
                $message = array("code" => 500, "message" => "Erro");
            }

            echo json_encode($message);
        }
    }

    public function printOrder($id = null){
        if($id == null)
            return;

        $data['order'] = $this->Order_model->with_itens()->get($id);

        $this->load->view('pedidos/pedido_print', $data);
    }
}
