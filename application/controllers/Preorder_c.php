<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Preorder_c extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->logged_in()){
            redirect("entrar","refresh");
        }

        $this->load->model("Preorder_model");
        $this->load->model("Preorderitem_model");
    }

    public function index()
    {
        $data['view'] = 'pedidos/prepedidos/list';

        $data["pedidos"] = $this->Preorder_model->with_itens()->get_all();

        $this->load->view('pedidos/container_view', $data);
    }

    public function show($id = null){

        if($id == null){
            redirect('dashboard','refresh');
        }

        $data['order'] = $this->Preorder_model->with_itens()->get($id);
        $data['view'] = 'pedidos/prepedidos/view';

        $this->load->view('pedidos/container_view', $data);
    }

    public function add(){
        if($_POST){
            $data_order = array(
                "name" => $_POST["name_order"],
                "period" => $_POST["preorder_period"],
                "id_client" => $_POST["id_client"],
                "id_driver" => $_POST["id_driver"],
                "driver_name" => $_POST["driver_name"],
                "client_name" => $_POST["client_name"],
                "production_time" => $_POST["production_time"],
                "delivery_time" => $_POST["delivery_time"],
                "payment_type" => $_POST["payment_type"],
                "value" => $_POST["real_value"]
                );

            $id = $this->Preorder_model->insert($data_order);

            if($id){
                for($i= 0; $i < count($_POST["id_item"]); $i++) {
                    $data_order_item = array(
                        "id_preorder" => $id,
                        "quantity" =>$_POST["quantity_expected"][$i],
                        "value" =>$_POST["single_value"][$i],
                        "id_item" =>$_POST["id_item"][$i],
                        "description" =>$_POST["item_description"][$i],
                        "observation_item" =>$_POST["observation_item"][$i]
                    );

                    $id_item = $this->Preorderitem_model->insert($data_order_item);
                }
            }
            redirect("preorder/".$id,'refresh');
        }

        $data['view'] = 'pedidos/prepedidos/new';

        $this->load->view('pedidos/container_view', $data);
    }

    public function edit($id = null){
        if($id == null){
            redirect('dashboard','refresh');
        }

        if($_POST){
            $data_order = array(
                "name" => $_POST["name_order"],
                "period" => $_POST["preorder_period"],
                "id_client" => $_POST["id_client"],
                "id_driver" => $_POST["id_driver"],
                "driver_name" => $_POST["driver_name"],
                "client_name" => $_POST["client_name"],
                "production_time" => $_POST["production_time"],
                "delivery_time" => $_POST["delivery_time"],
                "payment_type" => $_POST["payment_type"],
                "value" => $_POST["real_value"]
            );

            $updated = $this->Preorder_model->update($data_order, $id);

            if($updated){
                if(array_key_exists("id_item",$_POST)) {
                    for ($i = 0; $i < count($_POST["id_item"]); $i++) {
                        $data_order_item = array(
                            "id_preorder" => $id,
                            "quantity" => $_POST["quantity_expected"][$i],
                            "value" => $_POST["single_value"][$i],
                            "id_item" => $_POST["id_item"][$i],
                            "description" =>$_POST["item_description"][$i],
                            "observation_item" =>$_POST["observation_item"][$i]
                        );

                        $id_item = $this->Preorderitem_model->insert($data_order_item);
                    }
                }

                foreach($_POST as $key => $value){
                    if(strpos($key, "itemexclusion_") !== false){
                        $id_to_delete = explode("_", $key);

                        $file_to_delete = $this->Preorderitem_model->get($id_to_delete[1]);

                        if($file_to_delete != false){
                            $this->Preorderitem_model->delete($id_to_delete[1]);
                        }
                    }
                }
            }
        }

        $data['order'] = $this->Preorder_model->with_itens()->get($id);

        $data['view'] = 'pedidos/prepedidos/edit';

        $this->load->view('pedidos/container_view', $data);
    }

    public function searchPreorder(){
        if($_POST){

            $orders = $this->Preorder_model->with_itens()->get_all();

            echo json_encode($orders);
        }
    }

    public function getPreOrder(){
        if($_POST){

            $order = $this->Preorder_model->with_itens()->get($_POST["query"]);

            echo json_encode($order);
        }
    }

    public function getItens(){

        $id_cliente = "0";

        $query = "";

        if($_POST) {
            if (isset($_POST["id_cliente"]) && !empty($_POST["id_cliente"])) {
                $id_cliente = $_POST["id_cliente"];
            }

            if (isset($_POST["query"]) && !empty($_POST["query"])) {
                $query = $_POST["query"];
            }
        }

        $data = array(
            "id_cliente" => $id_cliente,
            "id_usuario" => "0",
            "texto" => $query
        );

        $data_string = json_encode($data);

        // $ch = curl_init('http://intelliware2.ddns.net:8088/Datasnap/rest/TProduto/%22getAll%22/');
        $ch = curl_init('http://192.168.0.104:8088/Datasnap/rest/TProduto/%22getAll%22/');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = json_decode(curl_exec($ch), true);

//        $myKey = "608dc2359e5203ab";
//
//        $myIV = "582ddb1915ab9946";

        $myKey = "e13e3bf2b20fa5df";

        $myIV = "0489e7862c68edd3";

        $crypttext =  mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $myKey, base64_decode($result['result']) , MCRYPT_MODE_CBC, $myIV);

        $results = json_decode($crypttext, true);

        $data = array();

        foreach($results as $re){
            $data[] = array("id" => $re["id"],
                "ean" => $re["ean"],
                "descricao" => $re["descricao"],
                "preco" => number_format($re["preco"], 2, ",", ".")
            );
        }

        echo json_encode($data);
    }

    public function getClients(){

        $query = "";

        if(isset($_POST["query"]))
            $query = $_POST["query"];

        $data = array(
//            "id_cliente"=>"0",
//            "ativo"=>"1"
            "texto" => $query
        );

        $data_string = json_encode($data);

        // $ch = curl_init('http://intelliware2.ddns.net:8088/Datasnap/rest/TCliente/%22getAll%22/');
        $ch = curl_init('http://192.168.0.104:8088/Datasnap/rest/TCliente/%22getAll%22/');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = json_decode(curl_exec($ch), true);

//        $myKey = "608dc2359e5203ab";
//
//        $myIV = "582ddb1915ab9946";

        $myKey = "e13e3bf2b20fa5df";

        $myIV = "0489e7862c68edd3";

        $crypttext =  mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $myKey, base64_decode($result['result']) , MCRYPT_MODE_CBC, $myIV);

        $results = json_decode($crypttext, true);

        $data = array();

        foreach($results as $re){
            if($_POST) {
                if($_POST["query"] !== ""){
                    if (strripos($re["nome"], $_POST["query"]) !== false){
                        $data[] = array("id" => $re["id"] ,
                            "nome" => $re["fantasia"]
                        );
                    }
                }else{
                    $data[] = array("id" => $re["id"] ,
                        "nome" => $re["fantasia"]
                    );
                }
            }else{
                if (strripos($re["nome"], "rafael") > 0) {
                    $data[] = array("id" => $re["id"],
                        "nome" => $re["fantasia"]
                    );
                }
            }
        }

        echo json_encode($data);
    }
}
