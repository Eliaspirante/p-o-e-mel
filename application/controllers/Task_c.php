<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_c extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
            redirect("entrar", "refresh");
        }

        $this->load->model("Preorder_model");
        $this->load->model("Order_model");
        $this->load->model("Orderitem_model");
    }

    public function generateOrder(){
        $preorders = $this->Preorder_model->with_itens()->where("active","1")->get_all();

        if($preorders){
            $today = date("Y-m-d");

            foreach($preorders as $preorder) {
                $orders = $this->Order_model->where("created_at", ">=", $today." 00:00:00")->where("id_preorder", $preorder->id)->get_all();

                if($orders){

                }else{
                    $data = array(
                        "name" => $_POST["name_order"],
                        "period" => $_POST["preorder_period"],
                        "id_client" => $_POST["id_client"],
                        "id_driver" => $_POST["id_driver"],
                        "driver_name" => $_POST["driver_name"],
                        "client_name" => $_POST["client_name"],
                        "production_time" => $_POST["production_time"],
                        "delivery_time" => $_POST["delivery_time"],
                        "value" => $_POST["real_value"]
                    );

                    $id = $this->Order_model->insert($data);

                    foreach($preorder->itens as $item){
                        $data_order_item = array(
                            "id_preorder" => $id,
                            "quantity" =>$item,
                            "value" =>$item,
                            "id_item" =>$item,
                            "description" =>$item,
                        );

                        $this->Orderitem_model->insert($data_order_item);
                    }
                }
            }
        }
    }
}