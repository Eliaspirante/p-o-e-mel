<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver_c extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->logged_in()){
            redirect("entrar","refresh");
        }

        $this->load->model("Driver_model");
    }

    public function index()
    {
        $data['view'] = 'motoristas/list';

        $data["motoristas"] = $this->Driver_model->get_all();

        $this->load->view('motoristas/container_view', $data);
    }

    public function show($id = null){
        if($id == null){
            redirect('dashboard','refresh');
        }

        $data['motorista'] = $this->Driver_model->get($id);
        $data['view'] = 'motoristas/view';

        $this->load->view('motoristas/container_view', $data);
    }

    public function add(){
        if($_POST){

            $this->load->model("Bill_model");

            $driver_data = array(
                "name" => $_POST["name"],
            );

            $id = $this->Driver_model->insert($driver_data);

            redirect("drivers/".$id,'refresh');
        }

        $data['view'] = 'motoristas/new';

        $this->load->view('motoristas/container_view', $data);
    }

    public function edit($id = null){
        if($id == null){
            redirect('dashboard','refresh');
        }

        if($_POST){
            $driver_data = array(
                "name" => $_POST["name"],
            );

            $updated = $this->Driver_model->update($driver_data, $id);


        }

        $data['motorista'] = $this->Driver_model->get($id);

        $data['view'] = 'motoristas/edit';

        $this->load->view('motoristas/container_view', $data);
    }

    public function search(){
        if($_POST){
            $motoristas = $this->Driver_model->where("name","like '%".$_POST['query']."%'", "")->get_all();

            echo json_encode($motoristas);
        }
    }
}
