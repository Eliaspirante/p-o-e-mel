<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_Order_c extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        if(!$this->ion_auth->logged_in()){
            redirect("entrar","refresh");
        }

        $this->load->model("Purchase_Order_model");
        $this->load->model("Purchase_item_model");
    }

    public function index()
    {
        $data['view'] = 'compras/list';

        $data["compras"] = $this->Purchase_Order_model->with_itens()->get_all();

        $this->load->view('compras/container_view', $data);
    }

    public function show($id = null){
        if($id == null){
            redirect('dashboard','refresh');
        }

        $data['order'] = $this->Purchase_Order_model->with_itens()->get($id);
        $data['view'] = 'compras/view';

        $this->load->view('compras/container_view', $data);
    }

    public function add(){
        if($_POST){
            $data_order = array(
                "name" => $_POST["name_order"],
                "id_client" => $_POST["id_client"],
                "client_name" => $_POST["client_name"],
                "value" => $_POST["real_value"]
            );

            $id = $this->Purchase_Order_model->insert($data_order);

            if($id){
                for($i= 0; $i < count($_POST["id_item"]); $i++) {
                    $data_order_item = array(
                        "id_purchase" => $id,
                        "quantity" =>$_POST["quantity_expected"][$i],
                        "value" =>$_POST["single_value"][$i],
                        "id_item" =>$_POST["id_item"][$i],
                        "description" =>$_POST["item_description"][$i],
                    );

                    $id_item = $this->Purchase_item_model->insert($data_order_item);
                }
            }

            $this->load->model("Bill_model");

            $bill_data = array(
                "id_order" => $id,
                "name" => "Compra - ".$id." - ".$_POST["client_name"],
                "value" => $_POST["real_value"],
                "payday" => date("Y-m-d", strtotime("+1 day")),
                "type" => 1
            );

            $this->Bill_model->insert($bill_data);

            redirect("purchase_orders/".$id,'refresh');
        }

        $data['view'] = 'compras/new';

        $this->load->view('compras/container_view', $data);
    }

    public function edit($id = null){
        if($id == null){
            redirect('dashboard','refresh');
        }

        if($_POST){
            $data_order = array(
                "name" => $_POST["name_order"],
                "period" => $_POST["order_period"],
                "id_client" => $_POST["id_client"],
                "client_name" => $_POST["client_name"],
                "value" => $_POST["real_value"]
            );

            $updated = $this->Purchase_Order_model->update($data_order, $id);

            if($updated){
                if(array_key_exists("id_item",$_POST)) {
                    for ($i = 0; $i < count($_POST["id_item"]); $i++) {
                        $data_order_item = array(
                            "id_purchase" => $id,
                            "quantity" => $_POST["quantity_expected"][$i],
                            "value" => $_POST["single_value"][$i],
                            "id_item" => $_POST["id_item"][$i],
                            "description" => $_POST["item_description"][$i],
                        );

                        $id_item = $this->Purchase_item_model->insert($data_order_item);
                    }
                }

                foreach($_POST as $key => $value){
                    if(strpos($key, "itemexclusion_") !== false){
                        $id_to_delete = explode("_", $key);

                        $file_to_delete = $this->Purchase_item_model->get($id_to_delete[1]);

                        if($file_to_delete != false){
                            $this->Purchase_item_model->delete($id_to_delete[1]);
                        }
                    }
                }
            }
        }

        $data['order'] = $this->Purchase_Order_model->with_itens()->get($id);

        $data['view'] = 'compras/edit';

        $this->load->view('compras/container_view', $data);
    }
}
