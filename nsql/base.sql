CREATE TABLE `users_drivers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `purchase_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_client` int(20) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `value` REAL DEFAULT 0,
  `hour` varchar(255) NOT NULL,
  `period` int(1) DEFAULT 1 NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `purchase_orders_itens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_purchase` int(20) unsigned DEFAULT NULL,
  `id_item` int(20) unsigned DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` int(20) unsigned DEFAULT NULL,
  `value` REAL DEFAULT 0,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bills` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_client` int(20) unsigned DEFAULT NULL,
  `id_order` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `value` REAL DEFAULT 0,
  `payday` DATE DEFAULT NULL,
  `pay_at` DATE DEFAULT NULL,
  `type` int(1) DEFAULT 1 NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_client` int(20) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `value` REAL DEFAULT 0,
  `hour` varchar(255) NOT NULL,
  `period` int(1) DEFAULT 1 NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `orders_itens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(20) unsigned DEFAULT NULL,
  `id_item` int(20) unsigned DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` int(20) unsigned DEFAULT NULL,
  `value` REAL DEFAULT 0,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `preorders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_client` int(20) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `value` REAL DEFAULT 0,
  `period` int(1) DEFAULT 1 NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `preorders_itens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_preorder` int(11) unsigned DEFAULT NULL,
  `id_item` int(20) unsigned DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `quantity` int(20) unsigned DEFAULT NULL,
  `value` REAL DEFAULT 0,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/** ALTERAÇÕES **/
ALTER TABLE `orders` ADD COLUMN `production_time` varchar(255) NOT NULL;
ALTER TABLE `preorders` ADD COLUMN `production_time` varchar(255) NOT NULL;

ALTER TABLE `orders` ADD COLUMN `delivery_time` varchar(255) NOT NULL;
ALTER TABLE `preorders` ADD COLUMN `delivery_time` varchar(255) NOT NULL;

ALTER TABLE `orders` ADD COLUMN `id_driver` int(20) unsigned DEFAULT NULL;
ALTER TABLE `orders` ADD COLUMN `driver_name` varchar(255) DEFAULT NULL;
ALTER TABLE `preorders` ADD COLUMN `id_driver` int(20) unsigned DEFAULT NULL;
ALTER TABLE `preorders` ADD COLUMN `driver_name` varchar(255) DEFAULT NULL;

ALTER TABLE `orders` ADD COLUMN `observation_item` varchar(255) DEFAULT NULL;
ALTER TABLE `preorders` ADD COLUMN `observation_item` varchar(255) DEFAULT NULL;

/** Rodado **/
DROP TABLE IF EXISTS `groups`;

#
# Table structure for table 'groups'
#

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Dumping data for table 'groups'
#

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
     (1,'admin','Administrator'),
     (2,'members','Usuário Geral');



DROP TABLE IF EXISTS `users`;

#
# Table structure for table 'users'
#

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


#
# Dumping data for table 'users'
#

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
     ('1','127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@admin.com','',NULL,'1268889823','1268889823','1', 'Admin','istrador','ADMIN','0');


DROP TABLE IF EXISTS `users_groups`;

#
# Table structure for table 'users_groups'
#

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `uc_users_groups` UNIQUE (`user_id`, `group_id`),
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
     (1,1,1),
     (2,1,2);


DROP TABLE IF EXISTS `login_attempts`;

#
# Table structure for table 'login_attempts'
#

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
