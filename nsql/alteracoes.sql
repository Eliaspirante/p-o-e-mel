CREATE TABLE `jobs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `done` int(1) NOT NULL DEFAULT 0,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `orders` ADD COLUMN `order_done` int(1) DEFAULT 0;
ALTER TABLE `orders` ADD COLUMN `payment_type` varchar(255) DEFAULT NULL;
ALTER TABLE `orders` ADD COLUMN `id_preorder` int(11) DEFAULT NULL;

ALTER TABLE `preorders` ADD COLUMN `active` int(1) DEFAULT 0;
ALTER TABLE `preorders` ADD COLUMN `payment_type` varchar(255) DEFAULT NULL;


ALTER TABLE `orders` ADD COLUMN `order_address` varchar(255) DEFAULT NULL;
ALTER TABLE `preorders` ADD COLUMN `order_address` varchar(255) DEFAULT NULL;

ALTER TABLE `orders` ADD COLUMN `active` int(1) DEFAULT 0;
ALTER TABLE `orders_itens` ADD COLUMN `done` int(1) DEFAULT 0;